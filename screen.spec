Name:           screen
Epoch:          1
Version:        4.9.0
Release:        1
Summary:        A full-screen window manager
License:        GPLv3+
URL:            http://www.gnu.org/software/screen
Source0:        http://ftp.gnu.org/gnu/screen/screen-%{version}.tar.gz
Source1:        screen.pam

Patch1:         screen-4.3.1-screenrc.patch
Patch2:         screen-E3.patch
Patch3:         screen-4.3.1-suppress_remap.patch
Patch4:         screen-4.3.1-crypt.patch

BuildRequires:  automake autoconf gcc ncurses-devel texinfo
BuildRequires:  systemd
Requires:       shadow-utils
Requires(preun): info
Requires(post):  info

%description
Screen is a full-screen window manager that multiplexes
a physical terminal between several processes,typically
interactive shells. 

%package help
Summary:     help for %{name}
BuildArch:   noarch

%description help
%{summary}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
autoreconf -fiv

%configure \
	--enable-pam \
	--enable-colors256 \
	--enable-rxvt_osc \
	--enable-use-locale \
	--with-pty-mode=0620 \
	--with-sys-screenrc="%{_sysconfdir}/screenrc" \
	--with-socket-dir="%{_rundir}/screen"

%make_build

%install
%make_install

mv -f %{buildroot}/%{_bindir}/screen{-%{version},}

install -D -p -m 0644 etc/etcscreenrc %{buildroot}/%{_sysconfdir}/screenrc
install -D -p -m 0644 %{SOURCE1} %{buildroot}/%{_sysconfdir}/pam.d/screen

install -d -m 0755 %{buildroot}/%{_rundir}/screen
install -d -m 0755 %{buildroot}/%{_tmpfilesdir}

cat <<EOF > %{buildroot}/%{_tmpfilesdir}/screen.conf
d %{_rundir}/screen 0775 root screen
EOF

# delete unpacked file
rm -f %{buildroot}/%{_infodir}/dir

%pre
/usr/sbin/groupadd -g 84 -r -f screen

%post
if [ -f %{_infodir}/screen.info.gz ]; then
    /sbin/install-info %{_infodir}/screen.info.gz %{_infodir}/dir 2>/dev/null || :
fi

%preun
if [ $1 = 0 ]; then
    if [ -f %{_infodir}/screen.info.gz ]; then
        /sbin/install-info --delete %{_infodir}/screen.info.gz %{_infodir}/dir 2>/dev/null || :
    fi
fi

%files
%defattr(-,root,root)
%doc README ChangeLog doc/FAQ doc/README.DOTSCREEN
%license COPYING
%config(noreplace) %{_sysconfdir}/screenrc
%config(noreplace) %{_sysconfdir}/pam.d/screen
%{_tmpfilesdir}/screen.conf
%attr(2755,root,screen) %{_bindir}/screen
%attr(775,root,screen) %{_rundir}/screen
%{_datadir}/screen

%files help
%{_mandir}/man1/screen.*
%{_infodir}/screen.info*

%changelog
* Fri Oct 21 2022 hongjinghao <hongjinghao@huawei.com> - 1:4.9.0-1
- update to 4.9.0

* Sat Jun 19 2021 panxiaohe <panxiaohe@huawei.com> - 1:4.8.0-11
- remove '--enable-telnet' in configure

* Fri Jun 4 2021 panxiaohe <panxiaohe@huawei.com> - 1:4.8.0-10
- add systemd to BuildRequires to use _tmpfilesdir macro
- fix bogus dates in changelog

* Fri Feb 26 2021 lirui<lirui130@huawei.com> - 1:4.8.0-9
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix CVE-2021-26937 

* Mon Jul 27 2020 linwei<linwei54@huawei.com> - 1:4.8.0-8
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:modify release and changelog

* Thu Jul 16 2020 linwei<linwei54@huawei.com> - 1:4.8.0-7
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update screen to 4.8.0

* Wed Mar 18 2020 openEuler Buildteam <buildteam@openeuler.org> - 1:4.6.2-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix Null pointer reference

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:4.6.2-5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add requires and a directory

* Sat Oct 26 2019 shenyangyang <shenyangyang4@huawei.com> - 1:4.6.2-4
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:add build requires of texinfo to solve the problem of build

* Thu Sep 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:4.6.2-3
- Modify requires

* Thu Sep 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:4.6.2-2
- Adjust requires

* Sat Sep 7 2019 openEuler Buildteam <buildteam@openeuler.org> - 1:4.6.2-1
- Package init
